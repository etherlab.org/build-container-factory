#!/bin/sh

set -e
set -v

# build base image
echo Building $IMAGE_NAME:base...
docker build --pull \
    --build-arg FEDORA_VERSION=$FEDORA_VERSION \
    -t $IMAGE_NAME:latest \
    -t $IMAGE_NAME:base \
    -f Dockerfile.mingw .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:latest...
    docker push $IMAGE_NAME:latest
    echo Pushing $IMAGE_NAME:base...
    docker push $IMAGE_NAME:base
fi

# build Qt6 image with pdcom5
echo Building $IMAGE_NAME:pdcom5-qt6...
docker build --pull \
    --build-arg FEDORA_VERSION=$FEDORA_VERSION \
    -t $IMAGE_NAME:pdcom5-qt6 \
    -f Dockerfile.mingw-qt6 .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:pdcom5-qt6...
    docker push $IMAGE_NAME:pdcom5-qt6
fi

# build combined images

TARGET=pdcom5
echo Building $IMAGE_NAME:$TARGET...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:base \
    -t $IMAGE_NAME:$TARGET \
    -f Dockerfile.$TARGET .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:$TARGET...
    docker push $IMAGE_NAME:$TARGET;
fi

TARGET=qtpdcom
echo Building $IMAGE_NAME:$TARGET...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:pdcom5 \
    -t $IMAGE_NAME:$TARGET \
    -f Dockerfile.$TARGET .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:$TARGET...
    docker push $IMAGE_NAME:$TARGET;
fi

TARGET=qt6pdcom
echo Building $IMAGE_NAME:$TARGET...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:pdcom5-qt6 \
    -t $IMAGE_NAME:$TARGET \
    -f Dockerfile.$TARGET .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:$TARGET...
    docker push $IMAGE_NAME:$TARGET;
fi

TARGET=pdwidgets2
echo Building $IMAGE_NAME:$TARGET...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:qtpdcom \
    -t $IMAGE_NAME:$TARGET \
    -f Dockerfile.$TARGET .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:$TARGET...
    docker push $IMAGE_NAME:$TARGET;
fi

TARGET=qt6pdwidgets2
echo Building $IMAGE_NAME:$TARGET...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:qt6pdcom \
    -t $IMAGE_NAME:$TARGET \
    -f Dockerfile.$TARGET .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:$TARGET...
    docker push $IMAGE_NAME:$TARGET;
fi

TARGET=qmlpdwidgets2
echo Building $IMAGE_NAME:$TARGET...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:qtpdcom \
    -t $IMAGE_NAME:$TARGET \
    -f Dockerfile.$TARGET .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:$TARGET...
    docker push $IMAGE_NAME:$TARGET;
fi
