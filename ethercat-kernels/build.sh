#!/bin/sh

set -e
set -v

for FLAVOUR in k3.4-rt145 rtai xenomai2 xenomai3; do
    docker build --pull \
        -t $IMAGE_NAME:$FLAVOUR \
        -f Dockerfile.$FLAVOUR .
    if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
        docker push $IMAGE_NAME:$FLAVOUR
    fi
done
