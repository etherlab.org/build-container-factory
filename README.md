# EtherLab Build Environment Factory

This repository contains Dockerfiles to pre-build the developer toolchain.

Currently, two images based on OpenSuSE Leap 15.3 are available:

 - [base](registry.gitlab.com/etherlab.org/build-container-factory/leap-15.3:base),
 which contains clang, cmake and many dependencies.
 - [qt5](registry.gitlab.com/etherlab.org/build-container-factory/leap-15.3:qt5),
 which contains Qt5 and FLTK in addition.

You can find them in the [gitlab container registry](https://gitlab.com/etherlab.org/build-container-factory/container_registry).

## MinGW containers

There is an additional fedora-based container to build Qt5-based application on Win64 using MinGW.

## Qt6.6 android containers

Available for 32 and 64bit arm (armv7l and aarch64) and x86_64.
Based on [KDE CI Container](https://invent.kde.org/sysadmin/ci-images).
Qt is installed in `/home/user/android-<platform>-clang`.
The following environment variables are available:

 - `PD_PREFIX`: `/home/user/android-<platform>-clang`
 - `PD_ANDROID_API`: 21
 - `PD_ANDROID_ABI`: `armeabi-v7a`, `arm64-v8a` or `x86_64`
 - `ANDROID_NDK_ROOT`
 - `ANDROID_SDK_ROOT`

## How to add dependencies

If you need another tool or library in the toolchain, simply add them into
`leap/Dockerfile.base`.  But please remember to keep the base image small, add
a new image if necessary.
