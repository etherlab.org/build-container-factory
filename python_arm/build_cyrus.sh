#!/bin/sh

set -e

export TARGET=$(echo $TARGET_CC | sed s/-gcc//)

export PREFIX="/usr"

export CFLAGS="-fPIC"
export LDFLAGS=""

NOCONFIGURE=1 ./autogen.sh
./configure --host $TARGET --prefix=$PREFIX \
        --with-openssl=$TARGET_SYSROOT \
        --disable-shared --enable-static \
        --without-saslauthd \
        --without-authdaemond \
        --without-pwcheck \
        --disable-krb4 \
        --with-rc4 \
        --with-dblib=none \
        --enable-anon \
        --enable-cram \
        --enable-digest \
        --enable-ntlm \
        --enable-plain \
        --enable-login \
        --disable-gssapi \
        --disable-alwaystrue \
        --disable-httpform \
        --disable-otp \
        --without-sqlite \
        --disable-sample

make -j$(nproc) install DESTDIR=$TARGET_SYSROOT

# prepare a CMake Module for pdcom5 so that it finds all dependencies
# to bake all sasl stuff in

LIBDIR=$TARGET_SYSROOT/$PREFIX/lib
mkdir -p $TARGET_SYSROOT/$PREFIX/share/cmake/CyrusSASL/

EXTERNAL_LIBRARIES="-L$LIBDIR -lcrypt -lcrypto"
PLUGINS=$(basename -a $LIBDIR/sasl2/lib*.a | tr '\n' ' ')

cat - > $TARGET_SYSROOT/$PREFIX/share/cmake/CyrusSASL/FindCyrusSASL.cmake <<EOF
find_path(CyrusSASL_INCLUDE_DIR sasl/sasl.h)
find_library(CyrusSASL_LIBRARY NAMES "libsasl2.a")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CyrusSASL
  FOUND_VAR CyrusSASL_FOUND
  REQUIRED_VARS
    CyrusSASL_LIBRARY
    CyrusSASL_INCLUDE_DIR
)

if (CyrusSASL_FOUND AND NOT TARGET sasl2)
    set(CyrusSASL_LIBRARIES "\${CyrusSASL_LIBRARY}" $EXTERNAL_LIBRARIES)
    set(CyrusSASL_INCLUDE_DIRS "\${CyrusSASL_INCLUDE_DIR}")

    add_library(sasl2 STATIC IMPORTED)
    set_target_properties(sasl2 PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "\${CyrusSASL_INCLUDE_DIR}"
        IMPORTED_LOCATION "\${CyrusSASL_LIBRARY}"
    )
    get_filename_component(CyrusSASL_PLUGIN_DIR "\${CyrusSASL_LIBRARY}" DIRECTORY)
    set(CyrusSASL_PLUGIN_DIR "\${CyrusSASL_PLUGIN_DIR}/sasl2")
    foreach(plugin $PLUGINS)
        target_link_libraries(sasl2 INTERFACE "\${CyrusSASL_PLUGIN_DIR}/\${plugin}")
        list(APPEND CyrusSASL_LIBRARIES "\${CyrusSASL_PLUGIN_DIR}/\${plugin}")
    endforeach(plugin)
    target_link_libraries(sasl2 INTERFACE $EXTERNAL_LIBRARIES)
endif()

mark_as_advanced(
  CyrusSASL_INCLUDE_DIR
  CyrusSASL_LIBRARY
)
EOF
