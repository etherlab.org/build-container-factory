
set -e

# grap dependencies from libtool file
EXTERNAL_LIBRARIES=""
for i in $LIBDIR/libsasl2.la $LIBDIR/sasl2/lib*.la; do
    EXTERNAL_LIBRARIES+=$(awk 'match($0, /^dependency_libs='"'([^']+)'"'$/, a) {print a[1]}' < $i)
done
# deduplicate libs
declare -A uniq
for l in $EXTERNAL_LIBRARIES; do uniq[$l]=1; done
EXTERNAL_LIBRARIES="${!uniq[@]}"

PLUGINS=$(basename -a $LIBDIR/sasl2/lib*.a | tr '\n' ' ')

cat - <<EOF
find_path(CyrusSASL_INCLUDE_DIR sasl/sasl.h)
find_library(CyrusSASL_LIBRARY NAMES "libsasl2.a")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CyrusSASL
  FOUND_VAR CyrusSASL_FOUND
  REQUIRED_VARS
    CyrusSASL_LIBRARY
    CyrusSASL_INCLUDE_DIR
)

if (CyrusSASL_FOUND)
    set(CyrusSASL_LIBRARIES "\${CyrusSASL_LIBRARY}" $EXTERNAL_LIBRARIES)
    set(CyrusSASL_INCLUDE_DIRS "\${CyrusSASL_INCLUDE_DIR}")

    add_library(sasl2 STATIC IMPORTED)
    set_target_properties(sasl2 PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "\${CyrusSASL_INCLUDE_DIR}"
        IMPORTED_LOCATION "\${CyrusSASL_LIBRARY}"
    )
    get_filename_component(CyrusSASL_PLUGIN_DIR "\${CyrusSASL_LIBRARY}" DIRECTORY)
    set(CyrusSASL_PLUGIN_DIR "\${CyrusSASL_PLUGIN_DIR}/sasl2")
    foreach(plugin $PLUGINS)
        target_link_libraries(sasl2 INTERFACE "\${CyrusSASL_PLUGIN_DIR}/\${plugin}")
        list(APPEND CyrusSASL_LIBRARIES "\${CyrusSASL_PLUGIN_DIR}/\${plugin}")
    endforeach(plugin)
    target_link_libraries(sasl2 INTERFACE $EXTERNAL_LIBRARIES)
endif()

mark_as_advanced(
  CyrusSASL_INCLUDE_DIR
  CyrusSASL_LIBRARY
)
EOF
