#!/bin/sh

set -e
set -v

# build base image
echo Building $IMAGE_NAME:base...
docker build --pull \
    --build-arg LEAP_VERSION=$LEAP_VERSION \
    -t $IMAGE_NAME:latest \
    -t $IMAGE_NAME:base \
    -f Dockerfile.base .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:latest...
    docker push $IMAGE_NAME:latest
    echo Pushing $IMAGE_NAME:base...
    docker push $IMAGE_NAME:base
fi

# build derived image
for DERIVED_TAG in qt5 qt6 linux-syms pdcom pdserv; do
    echo Building $IMAGE_NAME:$DERIVED_TAG...
    docker build --build-arg BASE_IMAGE=$IMAGE_NAME:latest \
        -t $IMAGE_NAME:$DERIVED_TAG \
        -f Dockerfile.$DERIVED_TAG .
    if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
        echo Pushing $IMAGE_NAME:$DERIVED_TAG...
        docker push $IMAGE_NAME:$DERIVED_TAG;
    fi
done

# build combined images

echo Building $IMAGE_NAME:qt5-pdcom...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:qt5 \
    -t $IMAGE_NAME:qt5-pdcom \
    -f Dockerfile.pdcom .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:qt5-pdcom...
    docker push $IMAGE_NAME:qt5-pdcom;
fi

echo Building $IMAGE_NAME:qt6-pdcom...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:qt6 \
    -t $IMAGE_NAME:qt6-pdcom \
    -f Dockerfile.pdcom .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:qt6-pdcom...
    docker push $IMAGE_NAME:qt6-pdcom;
fi

echo Building $IMAGE_NAME:qtpdcom...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:qt5-pdcom \
    -t $IMAGE_NAME:qtpdcom \
    -f Dockerfile.qtpdcom .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:qtpdcom...
    docker push $IMAGE_NAME:qtpdcom;
fi

echo Building $IMAGE_NAME:qt6pdcom...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:qt6-pdcom \
    -t $IMAGE_NAME:qt6pdcom \
    -f Dockerfile.qt6pdcom .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:qt6pdcom...
    docker push $IMAGE_NAME:qt6pdcom;
fi

echo Building $IMAGE_NAME:qt5pdwidgets...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:qtpdcom \
    -t $IMAGE_NAME:qt5pdwidgets \
    -f Dockerfile.qtpdwidgets .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:qt5pdwidgets...
    docker push $IMAGE_NAME:qt5pdwidgets;
fi

echo Building $IMAGE_NAME:qt6pdwidgets...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:qt6pdcom \
    -t $IMAGE_NAME:qt6pdwidgets \
    -f Dockerfile.qt6pdwidgets .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:qt6pdwidgets...
    docker push $IMAGE_NAME:qt6pdwidgets;
fi

echo Building $IMAGE_NAME:pdserv-pdcom...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:pdserv \
    -t $IMAGE_NAME:pdserv-pdcom \
    -f Dockerfile.pdcom .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:pdserv-pdcom...
    docker push $IMAGE_NAME:pdserv-pdcom;
fi

echo Building $IMAGE_NAME:pdserv-pdcom-ethercat...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:pdserv-pdcom \
    -t $IMAGE_NAME:pdserv-pdcom-ethercat \
    -f Dockerfile.ethercat .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:pdserv-pdcom-ethercat...
    docker push $IMAGE_NAME:pdserv-pdcom-ethercat;
fi

# build texlive image
TAG=texlive
echo Building $IMAGE_NAME:$TAG...
docker build --pull \
    --build-arg LEAP_VERSION=$LEAP_VERSION \
    -t $IMAGE_NAME:$TAG \
    -f Dockerfile.$TAG .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:$TAG...
    docker push $IMAGE_NAME:$TAG
fi
