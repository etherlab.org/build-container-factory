#!/bin/sh

set -e
set -v

# build base image
echo Building $IMAGE_NAME:base...
docker build --pull \
    --build-arg LEAP_VERSION=$LEAP_VERSION \
    -t $IMAGE_NAME:latest \
    -t $IMAGE_NAME:base \
    -f Dockerfile.base .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:latest...
    docker push $IMAGE_NAME:latest
    echo Pushing $IMAGE_NAME:base...
    docker push $IMAGE_NAME:base
fi

# build derived image
for DERIVED_TAG in qt6 linux-syms pdcom; do
    echo Building $IMAGE_NAME:$DERIVED_TAG...
    docker build --build-arg BASE_IMAGE=$IMAGE_NAME:latest \
        -t $IMAGE_NAME:$DERIVED_TAG \
        -f ../leap/Dockerfile.$DERIVED_TAG .
    if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
        echo Pushing $IMAGE_NAME:$DERIVED_TAG...
        docker push $IMAGE_NAME:$DERIVED_TAG;
    fi
done

# build combined images
echo Building $IMAGE_NAME:qt6-pdcom...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:qt6 \
    -t $IMAGE_NAME:qt6-pdcom \
    -f ../leap/Dockerfile.pdcom .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:qt6-pdcom...
    docker push $IMAGE_NAME:qt6-pdcom;
fi

echo Building $IMAGE_NAME:qt6pdcom...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:qt6-pdcom \
    -t $IMAGE_NAME:qt6pdcom \
    -f ../leap/Dockerfile.qt6pdcom .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:qt6pdcom...
    docker push $IMAGE_NAME:qt6pdcom;
fi
