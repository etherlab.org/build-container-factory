#!/bin/sh

set -e
set -v

for PLATFORM in arm arm64 x86_64; do
    TARGET=$IMAGE_NAME_BASE/$PLATFORM
    echo Building $TARGET:qt6pdcom...
    docker build --pull \
        --build-arg BASE_IMAGE=$BASE_IMAGE/$PLATFORM \
        -t $TARGET:qt6pdcom \
        -t $TARGET:latest \
        -f Dockerfile.qt6-$PLATFORM .
    if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
        echo Pushing $TARGET...
        docker push -a $TARGET
    fi
done
