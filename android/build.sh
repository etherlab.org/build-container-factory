#!/bin/sh

set -e
set -v


# build qtpdcom image
TARGET=qtpdcom
echo Building $IMAGE_NAME:$TARGET...
docker build --pull \
    -t $IMAGE_NAME:$TARGET \
    -f Dockerfile .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:$TARGET...
    docker push $IMAGE_NAME:$TARGET
fi

# build derived image
TARGET=pdqmlwidgets
echo Building $IMAGE_NAME:$TARGET...
docker build --build-arg BASE_IMAGE=$IMAGE_NAME:qtpdcom \
    -t $IMAGE_NAME:$TARGET \
    -f Dockerfile.$TARGET .
if [ "x${CI_COMMIT_BRANCH}" == "xmaster" ]; then
    echo Pushing $IMAGE_NAME:$TARGET...
    docker push $IMAGE_NAME:$TARGET
fi
