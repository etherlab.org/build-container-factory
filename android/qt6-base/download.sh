#!/bin/bash

# org.tensorflow:tensorflow-lite-metadata:0.1.0-rc2

# https://dl.google.com/dl/android/maven2/org/tensorflow/tensorflow-lite-metadata/0.1.0-rc2/tensorflow-lite-metadata-0.1.0-rc2.pom'

DIR=$1

REPO=my_cache

URL1=https://dl.google.com/dl/android/maven2
URL2=https://plugins.gradle.org/m2/

# extra file

mkdir -p "$DIR/$REPO/com/android/tools/build/aapt2/7.4.1-8841542/"
curl -L -sSf -o "$DIR/$REPO/com/android/tools/build/aapt2/7.4.1-8841542/aapt2-7.4.1-8841542-linux.jar" https://dl.google.com/dl/android/maven2//com/android/tools/build/aapt2/7.4.1-8841542/aapt2-7.4.1-8841542-linux.jar 

if [ "$?" != 0 ]; then
    echo "could not download aapt2-7.4.1-8841542-linux.jar"
    exit 1
fi


while IFS=':' read -r prefix name version; do
    prefix_as_path=$(echo $prefix | sed 's;\.;/;g')
    mkdir -p "$DIR/$REPO/$prefix_as_path/$name/$version"
    if [ -r "$DIR/$REPO/$prefix_as_path/$name/$version/$name-$version.pom" ]; then
        continue
    fi
    curl -L -sf -o "$DIR/$REPO/$prefix_as_path/$name/$version/$name-$version.pom" "$URL1/$prefix_as_path/$name/$version/$name-$version.pom"
    if [ "$?" != 0 ]; then
        curl -L -sSf -o "$DIR/$REPO/$prefix_as_path/$name/$version/$name-$version.pom" "$URL2/$prefix_as_path/$name/$version/$name-$version.pom"
        if [ "$?" != 0 ]; then
            echo "$URL1/$prefix_as_path/$name/$version/$name-$version.pom"
            exit 1
        fi
    fi
    curl -L -sf -o "$DIR/$REPO/$prefix_as_path/$name/$version/$name-$version.jar" "$URL1/$prefix_as_path/$name/$version/$name-$version.jar"
    if [ "$?" != 0 ]; then
        curl -L -sf -o "$DIR/$REPO/$prefix_as_path/$name/$version/$name-$version.jar" "$URL2/$prefix_as_path/$name/$version/$name-$version.jar"
    fi
    if [[ $prefix =~ androidx ]]; then
        curl -L -sf -o "$DIR/$REPO/$prefix_as_path/$name/$version/$name-$version.aar" "$URL1/$prefix_as_path/$name/$version/$name-$version.aar"
        if [ "$?" != 0 ]; then
            curl -L -sf -o "$DIR/$REPO/$prefix_as_path/$name/$version/$name-$version.aar" "$URL2/$prefix_as_path/$name/$version/$name-$version.aar"
        fi
    fi
done
